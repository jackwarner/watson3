��          4      L       `   k   a   C   �   9    `   K  M   �                    One or more mandatory questions have not been answered. You cannot proceed until these have been completed. This is a controlled survey. You need a valid token to participate. Project-Id-Version: LimeSurvey language file
Report-Msgid-Bugs-To: http://translate.limesurvey.org/
POT-Creation-Date: 2013-09-27 22:05:37+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2015-02-08 18:11-0500
Language-Team: LimeSurvey Team <c_schmitz@users.sourceforge.net>
X-Poedit-KeywordsList: gT;ngT:1,2;eT;neT:1,2
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..\..\..\
X-Generator: Poedit 1.7.4
Plural-Forms: nplurals=2; plural=(n != 1);
Language: en
Last-Translator: 
X-Poedit-SearchPath-0: .
 One or more questions have not yet been answered. Please answer all questions before proceeding. This is a controlled survey. You need a valid token to administer the survey. 
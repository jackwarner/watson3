If you receive a message similar to ~/watson-install/tmp/runtime should be writable by the webserver (755 or 775).,
run chmod -R 775 on this folder (up to the permissions to 777 if 775 does not work).